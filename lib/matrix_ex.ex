defmodule MatrixEx do
  @moduledoc """
  Documentation for MatrixEx.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MatrixEx.hello()
      :world

  """
  def hello do
    :world
  end
end
